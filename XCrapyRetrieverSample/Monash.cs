﻿using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace XCrapyRetrieverSample
{
    public class Monash
    {
        Uri baseUri = null;
        List<Uri> _courseCategoryUrlList = new List<Uri>();
        List<XcrapUrlObj> _xcrapUrlList = new List<XcrapUrlObj>();
        List<BaseCourse> _xcrapCourseList = new List<BaseCourse>();
        string UniversityName = "University of Monash";
        DBHelper _dBHelper = new DBHelper();

        ScrapingBrowser browser = new ScrapingBrowser();

        public Monash(string baseUrl)
        {
            baseUri = new Uri(baseUrl);
            PopulateCourseCategoryUrlList();

            browser.AllowAutoRedirect = true;
            browser.AllowMetaRedirect = true;

            PopulateXcrapUrlList();

            //ScrapData();
        }

        private void SaveToDb()
        {
            _dBHelper.AddTempCourseList(_xcrapCourseList);
        }

        private void PopulateCourseCategoryUrlList()
        {
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Art%2C+Design+and+Architecture"));
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Arts%2C+Humanities+and+Social+Sciences"));
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Business"));
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Education"));
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Engineering"));
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Information+Technology"));
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Law"));
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Medicine%2C+Nursing+and+Health+Sciences"));
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Pharmacy"));
            _courseCategoryUrlList.Add(new Uri("https://www.monash.edu/study/courses/find-a-course?f.InterestAreas%7CcourseInterestAreas=Science"));
        }

        private void PopulateXcrapUrlList()
        {
            foreach (var uri in _courseCategoryUrlList)
            {
                try
                {

                    WebPage pageResult = browser.NavigateToPage(uri);
                    var abosultePath = uri.AbsolutePath;

                    string courseCategory = HttpUtility.UrlDecode(uri.Query.ToString()).Split('=')[1];

                    List<HtmlNode> nodeList = pageResult.Html.CssSelect(".search-tabs__item").ToList();

                    foreach (var item in nodeList)
                    {
                        var outerHtml = item.InnerHtml;
                        var regex = new Regex("<a [^>]*href=(?:'(?<href>.*?)')|(?:\"(?<href>.*?)\")", RegexOptions.IgnoreCase);
                        var url = regex.Matches(outerHtml).OfType<Match>().Select(m => m.Groups["href"].Value).FirstOrDefault();

                        var cat = url.Split('&')[0].Split('=')[1];

                        if (cat == "Graduate" || cat == "Undergraduate" || cat == "graduate" || cat == "undergraduate" || cat == "Double degrees")
                        {
                            var courseTypeUrl = baseUri.OriginalString + abosultePath + url;

                            WebPage pageResult2 = browser.NavigateToPage(new Uri(courseTypeUrl));
                            List<HtmlNode> nodeLis2 = pageResult.Html.CssSelect(".box-featured__heading-link").ToList();
                            foreach (var item2 in nodeLis2)
                            {
                                url = item2.Attributes["title"].Value;
                                _xcrapUrlList.Add(new XcrapUrlObj(courseCategory, url, cat));

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    continue;
                }
            }
        }

        public  async Task ScrapData()
        {
            foreach (var item in _xcrapUrlList)
            {
                try
                {
                    BaseCourse baseCourse = new BaseCourse();

                    WebPage pageResult = browser.NavigateToPage(item.XcrapUri);
                    var title = pageResult.Html.CssSelect(".page-header-courses").FirstOrDefault();
                    baseCourse.CourseName = title.ChildNodes[1].InnerText + " - " + title.ChildNodes[3].InnerText;
                    baseCourse.CourseType = item.CourseType;
                    baseCourse.CourseCategory = item.Category;
                    baseCourse.University = UniversityName;
                    baseCourse.Country = "Australia";

                    baseCourse.Overview = pageResult.Html.CssSelect(".course-page__summary-text").FirstOrDefault().InnerHtml.CleanInnerHtmlAscii();

                    HtmlNode courseData = pageResult.Html.CssSelect(".course-page__table-basic").FirstOrDefault();
                    var trs = courseData.SelectNodes("//tbody/tr");

                    foreach (var tr in trs)
                    {
                        if (tr.InnerText.Contains("Location"))
                        {
                            baseCourse.Location = tr.ChildNodes.Where(w => w.Name == "td").FirstOrDefault().InnerText.CleanInnerText();
                        }
                        else if (tr.InnerText.Contains("Duration"))
                        {
                            var duration = tr.ChildNodes.Where(w => w.Name == "td").FirstOrDefault().InnerText;
                            baseCourse.Duration = duration.CleanInnerText();
                        }
                        else if (tr.InnerText.Contains("Start"))
                        {
                            baseCourse.Intake = tr.ChildNodes.Where(w => w.Name == "td").FirstOrDefault().InnerText;
                        }
                    }
                    var entryReq = pageResult.Html.CssSelect(".tabs__entry-requirements").FirstOrDefault();
                    var feeGridList = entryReq.CssSelect(".grid-col-md-5-9").ToList();
                    string fee = "";
                    if (feeGridList.Count == 1)
                    {
                        var feeGrid = feeGridList[0];
                        if (feeGrid.SelectNodes("//div[3]/div[2]/p/strong") != null)
                        {
                            if (feeGrid.SelectNodes("//div[3]/div[2]/p/strong").Count == 1) fee = feeGrid.SelectNodes("//div[3]/div[2]/p/strong")[0].FirstChild.InnerText;
                            else if (feeGrid.SelectNodes("//div[3]/div[2]/p/strong").Count == 2) fee = feeGrid.SelectNodes("//div[3]/div[2]/p/strong")[1].FirstChild.InnerText; 
                        }
                    }
                    if (feeGridList.Count == 2)
                    {
                        var feeGrid = feeGridList[1];
                        if (feeGrid.SelectNodes("//div[2]/div[2]/p/strong") != null)
                        {
                            if (feeGrid.SelectNodes("//div[2]/div[2]/p/strong").Count == 1) fee = feeGrid.SelectNodes("//div[2]/div[2]/p/strong")[0].FirstChild.InnerText;
                            else if (feeGrid.SelectNodes("//div[2]/div[2]/p/strong").Count == 2) fee = feeGrid.SelectNodes("//div[2]/div[2]/p/strong")[1].FirstChild.InnerText; 
                        }
                    }

                    baseCourse.CourseFee = fee;
                    _dBHelper.AddTempCourse(baseCourse);
                    //_xcrapCourseList.Add(baseCourse);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    continue;
                }
            }

            //SaveToDb();
        }
    }
}
