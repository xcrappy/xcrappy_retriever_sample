//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XCrapyRetrieverSample
{
    using System;
    using System.Collections.Generic;
    
    public partial class course_temp
    {
        public long course_id { get; set; }
        public string course_name { get; set; }
        public string course_category { get; set; }
        public string course_type { get; set; }
        public string university { get; set; }
        public string course_fee { get; set; }
        public string duration { get; set; }
        public string location { get; set; }
        public string intake { get; set; }
        public string country { get; set; }
        public string overview { get; set; }
    }
}
