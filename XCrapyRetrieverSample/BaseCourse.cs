﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XCrapyRetrieverSample
{
    public class BaseCourse
    {
        public long? CourseID { get; set; }
        public string CourseName { get; set; }
        public string CourseCategory { get; set; }
        public string CourseType { get; set; }
        public string University { get; set; }
        public string CourseFee { get; set; }
        public string Duration { get; set; }
        public string Location { get; set; }
        public string Intake { get; set; }
        public string Country { get; set; }
        public string Overview { get; set; }

        public course_temp GetEntity()
        {
            course_temp entity = new course_temp();
            entity.course_name = CourseName;
            entity.course_category = CourseCategory;
            entity.course_type = CourseType;
            entity.university = University;
            entity.course_fee = CourseFee;
            entity.duration = Duration;
            entity.location = Location;
            entity.intake = Intake;
            entity.country = Country;
            entity.overview = Overview;
            return entity;
        }
    }
}
