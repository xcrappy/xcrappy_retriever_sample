﻿using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCrapyRetrieverSample;

namespace ScrapyRetreiverSample
{
    class Program
    {
        static void Main(string[] args)
        {
            StartScrapping();

            Console.ReadKey();
        }

        private static async void StartScrapping()
        {
            Console.WriteLine("UNI: Melbourne => Scrapping BEGIN");
            Melbourne uniMelbourne = new Melbourne("https://study.unimelb.edu.au");
            await uniMelbourne.ScrapData();
            Console.WriteLine("UNI: Melbourne => Scrapping END");

            Console.WriteLine("UNI: Monash => Scrapping BEGIN");
            Monash uniMonash = new Monash("https://www.monash.edu");
            await uniMonash.ScrapData();
            Console.WriteLine("UNI: Monash => Scrapping END");

            Console.WriteLine("UNI: Australian National University => Scrapping BEGIN");
            ANU uniANU = new ANU("https://programsandcourses.anu.edu.au");
            await uniANU.ScrapData();
            Console.WriteLine("UNI: Australian National University => Scrapping END");
        }
    }
}
