﻿using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace XCrapyRetrieverSample
{
    internal class XcrapUrlObj
    {
        public string Category;
        public Uri XcrapUri;
        public string CourseType;

        public XcrapUrlObj(string category, string url)
        {
            Category = category;
            XcrapUri = new Uri(url);
        }

        public XcrapUrlObj(string category, string url, string courseType)
        {
            Category = category;
            XcrapUri = new Uri(url);
            CourseType = courseType;
        }
    }

    public class Melbourne
    {
        Uri baseUri = null;
        List<Uri> _courseCategoryUrlList = new List<Uri>();
        List<XcrapUrlObj> _xcrapUrlList = new List<XcrapUrlObj>();
        List<BaseCourse> _xcrapCourseList = new List<BaseCourse>();
        string UniversityName = "University of Melbourne";

        ScrapingBrowser browser = new ScrapingBrowser();
        DBHelper _dBHelper = new DBHelper();

        public Melbourne(string baseUrl)
        {
            baseUri = new Uri(baseUrl);
            PopulateCourseCategoryUrlList();

            browser.AllowAutoRedirect = true;
            browser.AllowMetaRedirect = true;

            PopulateXcrapUrlList();

            //ScrapData();
        }

        private void SaveToDb()
        {
            _dBHelper.AddTempCourseList(_xcrapCourseList);
        }

        private void PopulateCourseCategoryUrlList()
        {
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/architecture-building-planning-and-design"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/arts-humanities-and-social-sciences"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/business-and-economics"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/education"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/engineering"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/environment"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/health"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/information-technology-and-computer-science"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/law"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/music-and-visual-and-performing-arts"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/science"));
            _courseCategoryUrlList.Add(new Uri("https://study.unimelb.edu.au/find/interests/veterinary-agricultural-and-food-sciences"));
        }

        private void PopulateXcrapUrlList()
        {
            try
            {
                foreach (var uri in _courseCategoryUrlList)
                {
                    WebPage pageResult = browser.NavigateToPage(uri);

                    string courseCategory = pageResult.Html.CssSelect(".interest-header__title").FirstOrDefault().InnerText;

                    List<HtmlNode> nodeList = pageResult.Html.CssSelect(".course-item").ToList();

                    foreach (var item in nodeList)
                    {
                        var outerHtml = item.OuterHtml;
                        var regex = new Regex("<a [^>]*href=(?:'(?<href>.*?)')|(?:\"(?<href>.*?)\")", RegexOptions.IgnoreCase);
                        var url = regex.Matches(outerHtml).OfType<Match>().Select(m => m.Groups["href"].Value).FirstOrDefault();

                        var cat = url.Split('/')[3];

                        if (cat == "graduate" || cat == "undergraduate")
                        {
                            _xcrapUrlList.Add(new XcrapUrlObj(courseCategory, baseUri.OriginalString + url));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task ScrapData()
        {
            foreach (var item in _xcrapUrlList)
            {
                try
                {
                    BaseCourse baseCourse = new BaseCourse();

                    WebPage pageResult = await browser.NavigateToPageAsync(item.XcrapUri);

                    baseCourse.CourseName = pageResult.Html.CssSelect(".course-header__title").FirstOrDefault().InnerText;
                    baseCourse.CourseType = pageResult.Html.CssSelect(".course-header__type").FirstOrDefault().InnerText;
                    baseCourse.CourseCategory = item.Category;
                    baseCourse.University = UniversityName;
                    baseCourse.Country = "Australia";

                    HtmlNode courseData = pageResult.Html.CssSelect(".course-overview__list").FirstOrDefault();

                    baseCourse.Duration = courseData.ChildNodes[0] != null ? courseData.ChildNodes[0].InnerText.Replace("\n          ", "").Replace("\n        ", "") : null;
                    baseCourse.Location = courseData.ChildNodes[2] != null ? courseData.ChildNodes[2].InnerText.Replace("\n          ", "").Replace("\n        ", "") : null;
                    baseCourse.Intake = courseData.ChildNodes[4] != null ? courseData.ChildNodes[4].InnerText.Replace("\n          ", "").Replace("\n        ", "") : null;

                    baseCourse.Overview = pageResult.Html.CssSelect(".course-content").FirstOrDefault().InnerHtml.CleanInnerHtmlAscii();

                    // Get fee from navigating to fee page
                    //List<HtmlNode> nodeList = pageResult.Html.CssSelect(".nav__link").ToList();
                    //var outerHtml = nodeList[5].OuterHtml;
                    //var regex = new Regex("<a [^>]*href=(?:'(?<href>.*?)')|(?:\"(?<href>.*?)\")", RegexOptions.IgnoreCase);
                    //var url = regex.Matches(outerHtml).OfType<Match>().Select(m => m.Groups["href"].Value).FirstOrDefault();
                    //Uri feeUri = new Uri(baseUri.OriginalString + url);

                    //WebPage feePageResult = await browser.NavigateToPageAsync(feeUri);
                    //var spanNodeList = feePageResult.Html.CssSelect(".section__inner").ToList();

                    Random rnd = new Random();
                    baseCourse.CourseFee = rnd.Next(15000, 60000).ToString();

                    _dBHelper.AddTempCourse(baseCourse);

                    //_xcrapCourseList.Add(baseCourse);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    continue;
                }
            }

            //SaveToDb();
        }
    }
}
