﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ScrapySharp.Extensions;
using ScrapySharp.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XCrapyRetrieverSample
{
    public class ANU
    {
        Uri baseUri = null;
        List<Uri> _courseCategoryUrlList = new List<Uri>();
        List<XcrapUrlObj> _xcrapUrlList = new List<XcrapUrlObj>();
        List<BaseCourse> _xcrapCourseList = new List<BaseCourse>();
        string UniversityName = "Australian National University";
        static string selectedYear = "2020";
        List<ANUResponseCourse> _resCourseList = new List<ANUResponseCourse>();
        DBHelper _dBHelper = new DBHelper();

        ScrapingBrowser browser = new ScrapingBrowser();

        Uri reqUri = new Uri("https://programsandcourses.anu.edu.au/data/ProgramSearch/GetPrograms?q=&client=anu_frontend&proxystylesheet=anu_frontend&site=default_collection&btnG=Search&filter=0&client=anu_frontend&proxystylesheet=anu_frontend&site=default_collection&btnG=Search&filter=0&AppliedFilter=FilterByPrograms&ShowAll=true&PageIndex=0&MaxPageSize=20&PageSize=Infinity&InitailSearchRequestedFromExternalPage=true&SelectedYear=" + selectedYear + "&CollegeName=All+Colleges&ModeOfDelivery=All+Modes");

        public ANU(string baseUrl)
        {
            baseUri = new Uri(baseUrl);
            browser.AllowAutoRedirect = true;
            browser.AllowMetaRedirect = true;

            PopulateXcrapUrlList();
            //ScrapData();
        }

        private void SaveToDb()
        {
            _dBHelper.AddTempCourseList(_xcrapCourseList);
        }

        private void PopulateXcrapUrlList()
        {
            try
            {
                var result = browser.AjaxDownloadString(reqUri);
                var jObject = JObject.Parse(result);
                var jsonResult = JsonConvert.DeserializeObject<List<ANUResponseCourse>>(jObject.SelectToken("Items").ToString());

                _resCourseList.AddRange(jsonResult.Where(w => w.AcademicCareer.Contains("Undergraduate") ||
                                                        w.AcademicCareer.Contains("Research") ||
                                                        w.AcademicCareer.Contains("Postgraduate")).ToList());

                foreach (var course in _resCourseList)
                {
                    var url = baseUri.OriginalString + "/" + selectedYear + "/program/" + course.AcademicPlanCode;
                    XcrapUrlObj obj = new XcrapUrlObj("", url, course.AcademicCareer);
                    _xcrapUrlList.Add(obj);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task ScrapData()
        {
            foreach (var item in _xcrapUrlList)
            {
                try
                {
                    BaseCourse baseCourse = new BaseCourse();
                    var accCode = item.XcrapUri.AbsoluteUri.Split('/')[5];
                    var resCourse = _resCourseList.Where(w => w.AcademicPlanCode == accCode).FirstOrDefault();
                    baseCourse.CourseName = resCourse.ProgramName;
                    baseCourse.CourseType = resCourse.AcademicCareer;
                    baseCourse.University = UniversityName;
                    baseCourse.Intake = resCourse.ProgramAcademicYear;
                    baseCourse.Country = "Australia";

                    WebPage pageResult = browser.NavigateToPage(item.XcrapUri);

                    baseCourse.Overview = pageResult.Html.CssSelect(".body__inner").FirstOrDefault().InnerHtml.CleanInnerHtmlAscii();

                    baseCourse.Duration = pageResult.Html.CssSelect(".degree-summary__requirements").FirstOrDefault().SelectNodes("//li[1]/span[2]").FirstOrDefault().InnerText;

                    baseCourse.CourseCategory = pageResult.Html.CssSelect(".degree-summary__code-heading")
                        .Where(w => w.InnerText.Contains("Field of Education")).FirstOrDefault()
                        .ParentNode.CssSelect(".degree-summary__code-text").FirstOrDefault()
                        .InnerText;

                    baseCourse.CourseFee = pageResult.Html.CssSelect("#indicative-fees__international").FirstOrDefault()
                        .ChildNodes.Where(w => w.Name == "dl").FirstOrDefault()
                        .ChildNodes.Where(w => w.Name == "dd").FirstOrDefault()
                        .InnerText;

                    _dBHelper.AddTempCourse(baseCourse);
                    //_xcrapCourseList.Add(baseCourse);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    continue;
                }
            }

            //SaveToDb();
        }
    }

    class ANUResponseCourse
    {
        public string AcademicPlanCode { get; set; }
        public string ProgramName { get; set; }
        public string AcademicCareer { get; set; }
        public string ProgramAcademicYear { get; set; }
        public double? Duration { get; set; }
        public string Categories { get; set; }
    }
}
