﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XCrapyRetrieverSample
{
    public class DBHelper
    {
        xcrapy_sample_masterEntities dbContext = new xcrapy_sample_masterEntities();

        //public bool AddCourse(List<BaseCourse> courseList)
        //{
        //    using (var dbTransactionContext = dbContext.Database.BeginTransaction())
        //    {
        //        foreach (var item in courseList)
        //        {
        //            course courseObj = new course();
        //            courseObj.course_name = item.CourseName;
        //            courseObj.course_type = GetCourseTypeID(item.CourseName);
        //            courseObj.xxbt_course_category = 
        //        }
        //    }
        //}

        public void AddTempCourseList(List<BaseCourse> courseList)
        {
            using (var dbTransactionContext = dbContext.Database.BeginTransaction())
            {
                try
                {
                    foreach (var course in courseList)
                    {
                        course_temp tempCourse = course.GetEntity();
                        dbContext.course_temp.Add(tempCourse);
                    }

                    dbContext.SaveChanges();
                    dbTransactionContext.Commit();
                }
                catch (Exception ex)
                {
                    dbTransactionContext.Rollback();
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void AddTempCourse(BaseCourse course)
        {
            using (var dbTransactionContext = dbContext.Database.BeginTransaction())
            {
                try
                {
                    course_temp tempCourse = course.GetEntity();
                    dbContext.course_temp.Add(tempCourse);

                    dbContext.SaveChanges();
                    dbTransactionContext.Commit();
                }
                catch (Exception ex)
                {
                    dbTransactionContext.Rollback();
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public short? GetCourseTypeID(string courseType)
        {
            using (var dbTransactionContext = dbContext.Database.BeginTransaction())
            {
                return dbContext.xxbt_course_type.Where(w => w.description == courseType).FirstOrDefault().id;
            }
        }

        public short? GetCourseCategoryID(string courseCat)
        {
            using (var dbTransactionContext = dbContext.Database.BeginTransaction())
            {
                return dbContext.xxbt_course_category.Where(w => w.description == courseCat).FirstOrDefault().id;
            }
        }
    }
}
