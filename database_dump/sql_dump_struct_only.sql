/*
SQLyog Community v13.1.1 (64 bit)
MySQL - 8.0.12 : Database - xcrapy_sample_master
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`xcrapy_sample_master` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;

USE `xcrapy_sample_master`;

/*Table structure for table `country` */

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(64) NOT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_country_is_delete_xxbt_yesno_id` (`is_delete`),
  CONSTRAINT `fk_country_is_delete_xxbt_yesno_id` FOREIGN KEY (`is_delete`) REFERENCES `xxbt_yesno` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `course` */

DROP TABLE IF EXISTS `course`;

CREATE TABLE `course` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(128) NOT NULL,
  `course_fee` decimal(20,0) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `duration` int(10) DEFAULT NULL COMMENT 'in months',
  `university` bigint(20) DEFAULT NULL,
  `course_category` smallint(6) DEFAULT NULL COMMENT 'whether Science, Eng',
  `course_type` smallint(6) DEFAULT NULL COMMENT 'UG, Masters',
  `is_delete` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_university_id` (`university`),
  KEY `fk_course_category_xxbt_course_category_id` (`course_category`),
  KEY `fk_course_type_xxbt_course_type_id` (`course_type`),
  KEY `fk_is_delete_xxbt_yesno_id` (`is_delete`),
  CONSTRAINT `fk_course_category_xxbt_course_category_id` FOREIGN KEY (`course_category`) REFERENCES `xxbt_course_category` (`id`),
  CONSTRAINT `fk_course_type_xxbt_course_type_id` FOREIGN KEY (`course_type`) REFERENCES `xxbt_course_type` (`id`),
  CONSTRAINT `fk_is_delete_xxbt_yesno_id` FOREIGN KEY (`is_delete`) REFERENCES `xxbt_yesno` (`id`),
  CONSTRAINT `fk_university_id` FOREIGN KEY (`university`) REFERENCES `university` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `course_temp` */

DROP TABLE IF EXISTS `course_temp`;

CREATE TABLE `course_temp` (
  `course_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(256) DEFAULT NULL,
  `course_category` varchar(256) DEFAULT NULL,
  `course_type` varchar(256) DEFAULT NULL,
  `university` varchar(128) DEFAULT NULL,
  `course_fee` varchar(256) DEFAULT NULL,
  `duration` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `location` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `intake` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `country` varchar(256) DEFAULT NULL,
  `overview` text,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1508 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `university` */

DROP TABLE IF EXISTS `university`;

CREATE TABLE `university` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `university_name` varchar(64) NOT NULL,
  `rank` int(20) DEFAULT NULL,
  `country` bigint(20) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_country_id` (`country`),
  KEY `fk_university_is_delete_xxbt_yesno_id` (`is_delete`),
  CONSTRAINT `fk_country_id` FOREIGN KEY (`country`) REFERENCES `country` (`id`),
  CONSTRAINT `fk_university_is_delete_xxbt_yesno_id` FOREIGN KEY (`is_delete`) REFERENCES `xxbt_yesno` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `xxbt` */

DROP TABLE IF EXISTS `xxbt`;

CREATE TABLE `xxbt` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `description` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `xxbt_course_category` */

DROP TABLE IF EXISTS `xxbt_course_category`;

CREATE TABLE `xxbt_course_category` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `description` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `xxbt_course_type` */

DROP TABLE IF EXISTS `xxbt_course_type`;

CREATE TABLE `xxbt_course_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `description` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `xxbt_yesno` */

DROP TABLE IF EXISTS `xxbt_yesno`;

CREATE TABLE `xxbt_yesno` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `description` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
